$(document).ready(function(){

	
	// coupon
	$('#enterCode').click(function(){
		$('.step1').hide();
		$('.step2').fadeIn();
	});
	
	$('.submitCode').click(function(){
		if($('.codeField').val() == 'ERROR')
		{
			$('.step2').hide();
			$('.step3').show();
		}
		else {
			$('.coupon').hide();
			$('.showDiscount').show();
		}
	});
	

	// popovers
	$('a.satisf').webuiPopover({
		content: 'Aenean lacinia bibendum nulla sed consectetur. Donec ullamcorper nulla non metus auctor fringilla. Sed posuere consectetur est at lobortis. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.',
		width: 300
	});
	
	$('a.returns').webuiPopover({
		content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean lacinia bibendum nulla sed consectetur. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum.',
		width: 300
	});
	
	$('a.ebook').webuiPopover({
		content: 'Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Aenean lacinia bibendum nulla sed consectetur. Nullam quis risus eget urna mollis ornare vel eu leo. Fusce dapibus, tellus ac cursus commodo, ut fermentum massa justo sit amet risus.',
		width: 300
	});
	
	$('a.ccvhelp').webuiPopover({
		content: 'The verification number is a 3-digit number printed on the back of your card. It appears after and to the right of your card number. For American Express, 4 digits on the front of your card.',
		width: 300
	});


	// modal popup init scripts
	$('#review1').magnificPopup({type:'iframe'});
	
	$('#review2').magnificPopup({
		type: 'iframe'
	});
	
	$('#sizeChart').magnificPopup({
		type: 'iframe'
	});
	
	$('#terms, #shipping, #privacy').magnificPopup({
		type: 'iframe'
	});
	
	
	// increase-decrease cart amount
	$('a.incr').on('click',function(){
	    var $qty=$(this).closest('div.amnt').find('input');
	    var currentVal = parseInt($qty.val());
	    if (!isNaN(currentVal)) {
	        $qty.val(currentVal + 1);
	    }
	});
	$('a.decr').on('click',function(){
	    var $qty=$(this).closest('div.amnt').find('input');
	    var currentVal = parseInt($qty.val());
	    if (!isNaN(currentVal) && currentVal > 0) {
	        $qty.val(currentVal - 1);
	    }
	});
	
	
	// switch billing address
	$('#billingAddr').change(function(){
		if ($(this).is(':checked')) {
			$('.billing-address .inner').slideUp(100);
		}
		else {
			$('.billing-address .inner').slideDown(100);
		}
	});
	
	
	// switch paymeth
	var pmType;
	$('.paymeth').click(function(){
		$('.payment-methods a.paymeth').removeClass('active');
		$(this).addClass('active');
		pmType = $(this).attr('name');
		$('#paymethType').val(pmType);
	});
	
	
	// scroll to anchors
	$('a[href^="#"]').on('click',function (e) {
	    e.preventDefault();
	
	    var target = this.hash;
	    var $target = $(target);
	
	    $('html, body').stop().animate({
	        'scrollTop': $target.offset().top - 86
	    }, 900, 'swing');
	});


});